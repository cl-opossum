;;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10; package: cl-user -*-
;;;; **************************************************************************************
;;;;
;;;; (c) 2008 Utz-Uwe Haus, <lisp@uuhaus.de>
;;;;
;;;; $Id$
;;;;
;;;; This code is free software; you can redistribute it and/or modify
;;;; it under the terms of the version 3 of the GNU General
;;;; Public License as published by the Free Software Foundation, as
;;;; clarified by the prequel found in LICENSE.Lisp-GPL-Preface.
;;;;
;;;; This code is distributed in the hope that it will be useful, but
;;;; without any warranty; without even the implied warranty of
;;;; merchantability or fitness for a particular purpose. See the GNU
;;;; Lesser General Public License for more details.
;;;;
;;;; Version 3 of the GNU General Public License is in the file
;;;; LICENSE.GPL that was distributed with this file. If it is not
;;;; present, you can access it from
;;;; http://www.gnu.org/copyleft/gpl.txt (until superseded by a
;;;; newer version) or write to the Free Software Foundation, Inc., 59
;;;; Temple Place, Suite 330, Boston, MA 02111-1307 USA
;;;;
;;;; **************************************************************************************

;;; Load this file to generate documentation using David Lichteblau's atdoc
;;; (if you are adventurous add this file to the .asd file as depending on
;;;  the package itself)
(asdf:oos 'asdf:load-op 'atdoc)
;; we force a fresh compile-and-load cycle to make sure docstrings
;; are up-to-date
;; ugly: package docstring is not updated on allegro unless we
(ignore-errors (delete-package :opossum))

(asdf:oos 'asdf:compile-op 'opossum :force T)
(asdf:oos 'asdf:load-op 'opossum :force T)


(in-package :cl-user)

(atdoc:generate-documentation '(:OPOSSUM)
			      (directory-namestring
			       (make-pathname
				:directory (append
					    (pathname-directory
					     (asdf:system-definition-pathname
					      (asdf:find-system :opossum)))
					    '("web"))))
			      :index-title "OPOSSUM reference manual"
			      :heading "opossum"
			      :css "boxy.css")