;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10; -*-
;;;
;;; pegutils.lisp --- Utility functions for implementing PEG parsers

;; Copyright (C) 2008 Utz-Uwe Haus <lisp@uuhaus.de>
;; $Id$
;;
;; This code is free software; you can redistribute it and/or modify
;; it under the terms of the version 2.1 of the GNU Lesser General
;; Public License as published by the Free Software Foundation, as
;; clarified by the lisp prequel found in LICENSE.
;;
;; This code is distributed in the hope that it will be useful, but
;; without any warranty; without even the implied warranty of
;; merchantability or fitness for a particular purpose. See the GNU
;; Lesser General Public License for more details.
;;
;; Version 2.1 of the GNU Lesser General Public License is in the file
;; LICENSE that was distributed with this file. If it is not
;; present, you can access it from
;; http://www.gnu.org/copyleft/lgpl.txt (until superseded by a
;; newer version) or write to the Free Software Foundation, Inc., 59
;; Temple Place, Suite 330, Boston, MA 02111-1307 USA
;;
;; Commentary:

;; Some code here is inspired by the metapeg library of John Leuner

;;; Code:


(eval-when (:load-toplevel :compile-toplevel :execute)
  (declaim (optimize (speed 0)
		     #+cmu (safety 2)
		     #-cmu (safety 3)
		     (debug 3))))

(in-package #:opossum)

(defparameter *trace* nil "If non-nil, do extensive tracing of the parser functions.")


(defclass context ()
  (;; these slots are copied when cloning a context for recursion
   (input       :accessor input       :initarg :input       :initform nil
		:type string
		:documentation "The input string being parsed.")
   (dst-package :accessor dst-package :initarg :dst-package :initform nil
		:type package
		:documentation "The package into which symbols generated during the parse are interned.")
   (memotab     :accessor memotab     :initarg :memotab     :initform (make-hash-table :test #'equal)
		:type hash-table
		:documentation "Hash-table used to memoize parsing result. Keyed on (fun . offset) pairs.")
   ;; these slots are shared by all cloned copies of a context -- use only STORE-ACTION to guarantee consistency
   (actions     :accessor actions     :initarg :actions     :initform (make-list 1 :initial-element NIL)
		:type list
		:documentation "The list of actions accumulated during the parse.")
   (action-counter :accessor action-counter :initarg :action-counter :initform '(0)
		   :type (cons (integer 0) null)
		   :documentation "The counter of actions.")
   ;; these slots are what make a context unique
   (parent      :accessor parent      :initarg :parent      :initform nil
		:documentation "Parent context of this context.")
   (rule        :accessor rule        :initarg :rule        :initform nil
		:documentation "Rule name in this context.")
   (children    :accessor children    :initform nil)
   (value       :accessor value       :initarg :value       :initform nil
		:documentation "Accumulated value after successful matching of rule in this context.")
   (start-index :accessor start-index :initarg :start-index :initform nil
		:documentation "Position in INPUT where this context starts.")
   (end-index   :accessor end-index   :initarg :end-index   :initform nil
		:documentation "Position in INPUT where this context's match ends.")
   (depth       :accessor depth       :initarg :depth       :initform 0
		:documentation "How deep in the tree is this context?"))
  (:documentation "A parser context object."))

(defmethod print-object ((obj context) stream)
  (print-unreadable-object (obj stream :type T :identity NIL)
    (format stream "rule ~A (~S) value ~S (~D:~D)"
	    (rule obj) (children obj) (value obj) (start-index obj) (end-index obj))))

(defmethod store-action ((ctx context) action)
  "Store ACTION in context CTX."
  (let ((a (actions ctx)))
    (rplacd a (cons (car a) (cdr a)))
    (rplaca a action)))

(defvar *context* nil "The current parser context.")

(defun clone-ctx (ctx rule)
  "Create clone context of CTX for rule RULE."
  (make-instance 'context
		 :input (input ctx)
		 :dst-package (dst-package ctx)
		 :memotab (memotab ctx)
		 :actions (actions ctx)
		 :action-counter (action-counter ctx)
		 :parent ctx
		 :rule rule
		 :start-index (end-index ctx)
		 :depth (1+ (depth ctx))))

(defun ctx-failed-p (ctx)
  "Check whether CTX failed to match."
  (null (end-index ctx)))

(defun succeed (ctx value start-index end-index)
  "Mark CTX as successful: set VALUE and matched region START-INDEX:END-INDEX."
  (setf (value ctx) value)
  (setf (start-index ctx) start-index)
  (setf (end-index ctx) end-index)
  ;; (when *trace*
  ;;     (format *trace-output* "Matched: ~A (~D:~D)~%"
  ;; 	    (rule ctx) (start-index ctx) (end-index ctx)))
  ctx)

(defun fail ()
  "Return a failure context generate from *CONTEXT*."
  (let ((ctx (make-instance 'context
			    :input (input *context*)
			    :rule ':fail
			    :value (rule *context*)
			    :start-index (start-index *context*)
			    :end-index (end-index *context*)
			    ;; probably some of these copies can be saved
			    :dst-package (dst-package *context*)
			    :actions (actions *context*)
			    :action-counter (action-counter *context*)
			    :depth (1+ (depth *context*)))))
    ;; (when *trace*
    ;;       (format *trace-output* "(failed: ~A ~A ~A)~%"
    ;; 	      (value ctx) (start-index ctx) (end-index ctx)))
    ctx))

(defun find-memoized-value (name offset &optional (ctx *context*))
  "Return a memoized value for FUN at OFFSET, or NIL."
  (let ((res (gethash `(,name . ,offset) (memotab ctx))))
    (when *trace*
      (format *trace-output* "~vT~A memoized result.~%" (depth ctx) (if res "Found" "No")))
    res))

(defun memoizing (name offset result-ctx &optional (ctx *context*))
  (when *trace*
    (format *trace-output* "~vT(memoizing for ~A/~D)~%" (depth ctx) name offset))
  (setf (gethash `(,name . ,offset) (memotab ctx))
	result-ctx)
  result-ctx)

;;
(defun make-name (rule-string)
  "Create a symbol suitable for naming the parser function for rule RULE-STRING."
  (intern (concatenate 'string "parse-" rule-string)
	  (dst-package *context*)))

(defun make-action-name (&key ctx)
  "Return a symbol suitable to name the next action in the current *CONTEXT*."
  (incf (car (action-counter *context*)))
  (let ((aname (if ctx
		   (format nil "opossum-action-~D-srcpos-~D-~D"
			   (car (action-counter *context*))
			   (start-index ctx)
			   (end-index ctx))
		   (format nil "opossum-action-~D"
			   (car (action-counter *context*)))) ))
    (intern aname (dst-package *context*))))

(defun char-list-to-string (char-list)
  (coerce char-list 'string))

(defmacro build-parser-function (name parser)
  "Return a function of 1 argument, the offset in *CONTEXT*, parsing using the given PARSER."
  `(lambda (offset)
    ,(format nil "Parse a ~A at the given OFFSET." name)
    (let ((indent (depth *context*)))
      (when *trace* (format *trace-output* "~vTTrying to parse a ~A at pos ~D~%" indent ,name offset))
      (or (find-memoized-value ,name offset)
	  (let* ((*context* (clone-ctx *context* ,name))
		 (result (funcall ,parser offset)))
	    (unless result
	      (error "Parser function ~A did not return a value" ,parser))
	    (if (ctx-failed-p result)
		(progn
		  (when *trace* (format *trace-output* "~vT... no ~A at pos ~D~%" indent ,name offset))
		  (memoizing ,name offset (fail)))
		(progn
		  (when *trace* (format *trace-output* "~vT... found ~A at ~D:~D~%"
					indent
					,name (start-index result) (end-index result)))
		  (memoizing ,name offset (succeed *context* (value result) (start-index result) (end-index result))))))))))



(defun match-string (string)
  "Return a function of 1 argument, the offset in *CONTEXT*, that tries to match STRING at that position."
  #'(lambda (offset)
      (let ((input (input *context*))
	    (len (length string)))
	(if (and (>= (length input) (+ offset len))
		 (string= string input :start2 offset :end2 (+ offset len)))
	    (succeed (clone-ctx *context* 'opossum-string) string offset (+ offset (length string)))
	    (fail)))))

(defun match-char (char-list)
  #'(lambda (offset)
      (let ((input (input *context*)))
;; 	(when *trace*
;; 	  (format *trace-output* "match-char: looking for one of `~{~A~}'~%" char-list))
	(if (and (> (length input) offset)
		 (member (char input offset)
			 char-list :test #'char=))
	    (succeed (clone-ctx *context* 'opossum-char) (char input offset) offset (+ offset 1))
	    (fail)))))

(defun match-octal-char-code (i1 i2 i3)
  "Compare the character given by i3 + 8*i2 + 64*i1 to the next input character."
  (let ((c (+ i3 (* 8 i2) (* 64 i1))))
    #'(lambda (offset)
	(let ((input (input *context*)))
;; 	  (when *trace*
;; 	    (format *trace-output* "match-octal-char-code: looking for ~D~%" c))
	  (if (and (> (length input) offset)
		   (= (char-int (char input offset)) c))
	      (succeed (clone-ctx *context* 'opossum-char) (char input offset) offset (+ offset 1))
	      (fail))))))

(defun match-char-range (lower-char upper-char)
  "Match characters in the range between LOWER-CHAR and UPPER-CHAR (inclusive) as decided by CL:CHAR-CODE."
  #'(lambda (offset)
      (let ((input (input *context*)))
;; 	(when *trace*
;; 	  (format *trace-output* "match-char-range: looking for ~A-~A~%" lower-char upper-char))
	(if (and (> (length input) offset)
		 (let ((x (char-code (char input offset))))
		   (and (>= x (char-code lower-char))
			(<= x (char-code upper-char)))))
	    (succeed (clone-ctx *context* 'opossum-char-range) (char input offset) offset (+ offset 1))
	    (fail)))))

(defun match-any-char (&optional ignored)
  (declare (ignore ignored))
  #'(lambda (offset)
      "Match any character at OFFSET, fail only on EOF."
      (let ((input (input *context*)))
;; 	(when *trace*
;; 	  (format *trace-output* "match-any-char~%"))
	(if (< (1+ offset) (length input))
	    (succeed (clone-ctx *context* 'opossum-anychar) (char input offset) offset (+ offset 1))
	    (fail)))))

(defun match-char-class (char-class)
  "Regexp matching of next input character against [CHAR-CLASS] using cl-ppcre:scan."
  (declare (type string char-class))
  ;; FIXME: could use a pre-computed scanner
  (let ((cc (format nil "[~A]" char-class)))
    #'(lambda (offset)
	"Match next character at OFFSET against the characters in CHAR-CLASS."
	(let ((input (input *context*)))
;; 	  (when *trace*
;; 	    (format *trace-output* "match-char-class on ~A~%"))
	  (if (and (< (1+ offset) (length input))
		   (let ((c (char input offset)))
		     (cl-ppcre:scan cc (make-string 1 :initial-element c))))
	      (succeed (clone-ctx *context* 'opossum-charclass)
		       (char input offset) offset (+ offset 1))
	      (fail))))))

(defun fix-escape-sequences (char-list)
  "Iterate over the list CHAR-LIST, glueing adjacent #\\ #\n and #\\ #\t chars into
#\Newline and #\Tab."
  (cond
    ((null char-list) char-list)
    ((null (cdr char-list)) char-list)
    (T 
     (loop :with drop := nil
	   :for (c1 c2) :on char-list
	   :if drop
	   :do (setf drop nil)
	   :else
	   :collect (if (char= c1 #\\)
			(case c2
			  ((#\n) (setf drop T) #\Newline)
			  ((#\t) (setf drop T) #\Tab)
			  ((#\r) (setf drop T) #\Linefeed)
			  (otherwise c1))
			c1)
	   :end))))

;;
;; parsing combinator functions cribbed from libmetapeg
;;
(defun either (&rest parsers)
  "Produce a function that tries each of the functions in PARSERS sequentially until one succeeds and
returns the result of that function, or a failure context if none succeeded."
  #'(lambda (offset)
      (let ((*context* (clone-ctx *context* 'opossum-either)))
	;; (when *trace*
	;; 	  (format *trace-output* "either: ~A ~A~%" *context* parsers))
	(loop :for p :in parsers
	      :as result = (funcall p offset)
	      :when (not (ctx-failed-p result))
	      :return (succeed *context* (value result) offset (end-index result))
	      :finally (return (fail))))))


(defun optional (parser)
  #'(lambda (offset)
      (let ((*context* (clone-ctx *context* 'opossum-optional)))
;; 	(when *trace*
;; 	  (format *trace-output* "optional: ~A ~A~%" *context* parser))
	(let ((result (funcall parser offset)))
	  (if (ctx-failed-p result)
	      (succeed *context* 'optional offset offset)
	      (succeed *context* (value result) offset (end-index result)))))))

(defun follow (parser)
  #'(lambda (offset)
      (let ((*context* (clone-ctx *context* 'opossum-follow)))
;; 	(when *trace*
;; 	  (format *trace-output* "follow: ~A ~A~%" *context* parser))
	(let ((result (funcall parser offset)))
	  (if (ctx-failed-p result)
	      (fail)
	      (succeed *context* (value result)
		       ;; don't consume input
		       offset offset)))))) 

(defun many (parser)
  #'(lambda (offset)
      (let ((*context* (clone-ctx *context* 'opossum-many))
	    (start-offset offset)
	    children)
;; 	(when *trace*
;; 	  (format *trace-output* "many: ~A ~A~%" *context* parser))
	(loop :as result := (funcall parser offset)
	      :while (not (ctx-failed-p result))
	      :do (progn (push (value result) children)
			 (setf offset (end-index result)))
	      :finally (return (succeed *context* (nreverse children) start-offset offset))))))


(defun many1 (parser)
  #'(lambda (offset)
      (let* ((*context* (clone-ctx *context* 'opossum-many1))
	     (result (funcall parser offset)))
;; 	(when *trace*
;; 	  (format *trace-output* "many1: ~A ~A~%" *context* parser))
	(if (not (ctx-failed-p result))
	    (let ((result2 (funcall (many parser) (end-index result))))
	      (if (end-index result2)
		  (succeed *context* (cons (value result) (value result2)) offset (end-index result2))
		  (succeed *context* (value result) offset (end-index result))))
	    (fail)))))


(defun seq (&rest parsers)
  #'(lambda (offset)
      (assert (> (length parsers) 0))
      (let ((*context* (clone-ctx *context* 'opossum-seq))
	    (start-offset offset)
	    child-values
	    child-nodes)
;; 	(when *trace*
;; 	  (format *trace-output* "seq: ~A ~A~%" *context* parsers))
	;; run the parsers
	(loop :for p :in parsers
;; 	      :do (when *trace* (format *trace-output* " (seq ~A) trying ~A~%" *context* p))
	      :do (cond
		    ((consp p)
		     (push (succeed (clone-ctx *context* 'action) nil offset offset)  child-nodes)
		     (push p child-values)
		     (setf (children *context*) (reverse child-nodes)))
		    (T
		     (let ((result (funcall p offset)))
		       (if (end-index result)
			   (progn
			     (push result child-nodes)
			     (push (value result) child-values)
			     (setf offset (end-index result))
			     (setf (children *context*) (reverse child-nodes)))
			   (return (fail))))))
	      :finally (return (succeed *context* (reverse child-values) start-offset offset))))))

(defun negate (parser)
  #'(lambda (offset)
      "Return a successful context at OFFSET if PARSER succeeds, without advancing input position."
      (let ((*context* (clone-ctx *context* 'opossum-negate)))
	(let ((result (funcall parser offset)))
	  (if (ctx-failed-p result)
	      (succeed *context* 'negate offset offset) 
	      (fail))))))


;;
(defun read-stream (stream)
  "Read STREAM and return a string of all its contents."
  (let ((s ""))
    (loop :as line := (read-line stream nil nil)
	  :while line
	  :do (setq s (concatenate 'string s line)))
    s))

(defun read-file (file)
  "Read FILE and return a string of all its contents."
  (with-open-file (f file :direction :input)
    (let ((len (file-length f)))
      (if len
	  (let ((s (make-string len)))
	    (read-sequence s f)
	    s)
	  (read-stream f)))))

(defun make-default-dst-package (grammarfile)
  (let ((pkg (make-package (gensym "opossum-parser"))))
    (setf (documentation pkg 'cl:package)
	  (format T "Opossum parser for grammar ~A" grammarfile))
    pkg))

(defun get-iso-time ()
  "Return a string in ISO format for the current time"
  (multiple-value-bind (second minute hour date month year day daylight-p zone)
      (get-decoded-time)
    (declare (ignore day))
    (format nil "~4D-~2,'0D-~2,'0D-~2,'0D:~2,'0D:~2,'0D (UCT~@D)"
	    year month date
	    hour minute second
	    (if daylight-p (1+ (- zone)) (- zone)))))

(defun cleanup-action-code (code)
  "Remove trailing newlines  so that the string CODE can be printed nicely."
  (subseq code 0
	  (when (char= #\Newline (char code (1- (length code))))
	    (1+ (position #\Newline code :from-end T :test #'char/=)))))

(defmacro checking-parse (grammarfile parse-file-fun)
  (let ((res (gensym "resultctx")))
    `(let ((,res (funcall ,parse-file-fun ,grammarfile *package*)))
      (cond
	((ctx-failed-p ,res)
	 (format *error-output* "Failed to parse PEG grammar ~A~%" ,grammarfile)
	 (error "Parsing ~A failed: ~A" ,grammarfile ,res))
	((< (end-index ,res) (length (input ,res)))
	 (format *error-output* "Parsed only ~D characters of grammar ~A~%" (end-index ,res) ,grammarfile)
	 ,res)
	(T ,res)))))

(defun generate-parser-file (grammarfile dst-package dst-file &key start-rule (parse-file-fun (symbol-function 'opossum:parse-file)))
  "Create lisp code in DST-FILE that can be loaded to yield functions to parse using GRAMMARFILE in DST-PACKAGE.
DST-PACKAGE will contain the 3 functions PARSE-STRING, PARSE-FILE and PARSE-STREAM as exported entrypoints."
  (let* ((*package* (find-package dst-package))
	 (result (checking-parse grammarfile parse-file-fun))
	 ;; FIXME: check for complete parse
	 (*context* result)  ;; routines in pegutils.lisp expect *context* to be bound properly
	 (dpkg (intern (package-name dst-package) :keyword)))
    (let ((forms (transform (value result)))
	  (actions (actions result)))
      (with-open-file (s dst-file :direction :output :if-exists :supersede)
	(let ((*print-readably* T)
	      (*print-pretty* T)
	      (*print-circle* NIL))
	  (format s ";; This is a Common Lisp peg parser automatically generated by OPOSSUM -*- mode:lisp -*-~%")
	  (format s ";; generated from ~A on ~A~%" grammarfile (get-iso-time))
	  (prin1 `(eval-when (:load-toplevel :compile-toplevel :execute)
		   (declaim (optimize (speed 0) (safety 3) (debug 3))))
		 s)
	  (terpri s) 
	  (prin1 `(defpackage ,dpkg
		   (:use :cl :opossum)
		   (:export :parse-string :parse-file :parse-stream :*trace*))
		 s)
	  (terpri s)
	  (prin1 `(in-package ,dpkg) s) (terpri s)
	  ;; First form is taken to be the start rule
	  (let ((entryrule (or (and start-rule (make-name start-rule))
			       (and forms (cadr (first forms))))))
	    (if (not entryrule)
		(format *error-output* "Cannot find entry rule for parser")
		(progn
		  (when *trace*
		    (format *trace-output* "Inserting definitions for parser entry points through ~A~%"
			    entryrule))
		  (terpri s)
		  (prin1 `(defun parse-string (,(intern "s" dst-package) dst-package)
			   ,(format nil "Parse S using grammar ~A starting at ~A" grammarfile entryrule)
			   (let ((*context* (make-instance 'opossum:context
							   :dst-package dst-package
							   :input ,(intern "s" dst-package))))
			     (funcall (,entryrule) 0)))
			 s)
		  (terpri s)
		  (prin1 `(defun parse-file (,(intern "f" dst-package) dst-package)
			   ,(format nil "Parse file F using grammar ~A starting at ~A" grammarfile entryrule)
			   (parse-string (opossum:read-file ,(intern "f" dst-package)) dst-package))
			 s)
		  (terpri s)
		  (prin1 `(defun parse-stream (,(intern "stream" dst-package) dst-package)
			   ,(format nil "Parse stream F using grammar ~A starting at ~A" grammarfile entryrule)
			   (parse-string (opossum:read-stream ,(intern "stream" dst-package)) dst-package))
			 s)
		  (fresh-line s))))
	  (loop :for aform :in forms
		:do (when *trace* (format *trace-output* "Inserting form ~A~%" aform))
		:do (terpri s)
		:do (prin1 aform s)
		:do (fresh-line s))
	  (terpri s)
	  (prin1
	   `(defparameter ,(intern "*trace*" dst-package) nil
	     "When non-nil, the generated parser function log to cl:*trace-output*.")
	   s)
	  (terpri s)
	      
	  (loop :for (sym code) :in actions
		:when sym ;; the final action is named NIL because we push a
		;; NIL ahead of us in store-actions
		:do (when *trace* (format *trace-output* "Inserting defun for ~A~%" sym))
		:and :do (format s "~%(defun ~S (data)~% (declare (ignorable data) (type list data))~% ~A)~%"
				 sym (cleanup-action-code code))))))))

(defun generate-parser-package (grammarfile &key (dst-package (make-package (gensym "opossum-parser-")))
				start-rule (parse-file-fun (symbol-function 'opossum:parse-file)))
  "Create functions to parse using GRAMMARFILE in DST-PACKAGE, starting ar rule named HEAD.
DST-PACKAGE will contain the 3 functions PARSE-STRING, PARSE-FILE and PARSE-STREAM as exported entrypoints."
  (let* ((*package* dst-package)
	 (result (checking-parse grammarfile parse-file-fun))
	 ;; FIXME: check for complete parse
	 (*context* result)) ;; routines in pegutils.lisp expect *context* to be bound properly)
    (let ((forms (transform (value result)))
	  (actions (actions result)))
      (format *trace-output* "Injecting parser functions into ~A~%" dst-package)
      (break "~A, ~A" forms actions)
      (use-package '(:cl :opossum) dst-package)
      (let ((entryrule (or (and start-rule (make-name start-rule))
			   (and forms (cadr (first forms))))))
	(if (not entryrule)
	    (format *error-output* "Cannot find entry rule for parser")
	    (progn
	      (when *trace*
		(format *trace-output* "Inserting definitions for parser entry points through ~A~%"
			entryrule))
	      (intern (symbol-name
		       (compile 'parse-string
				`(lambda (,(intern "s" dst-package))
				  ,(format nil "Parse S using grammar ~A starting at ~A" grammarfile entryrule)
				  (let ((*context* (make-instance 'opossum:context
								  :dst-package ,dst-package
								  :input ,(intern "s" dst-package))))
				    (funcall (,entryrule) 0)))))
		      dst-package)
	      (intern (symbol-name
		       (compile 'parse-file
				`(lambda (,(intern "f" dst-package))
				  ,(format nil "Parse file F using grammar ~A starting at ~A" grammarfile entryrule)
				  (parse-string (opossum:read-file ,(intern "f" dst-package))))))
		      dst-package)
	      
	      (intern (symbol-name
		       (compile 'parse-stream
				`(lambda (,(intern "stream" dst-package))
				  ,(format nil "Parse stream F using grammar ~A starting at ~A" grammarfile entryrule)
				  (parse-string (opossum:read-stream ,(intern "stream" dst-package))))))
		      dst-package))))
      (intern "*TRACE*" dst-package)
      (setf (documentation (find-symbol "*TRACE*" dst-package) 'cl:variable)
	    "When non-nil, the generated parser function log to cl:*trace-output*.")
      (export '(:parse-string :parse-file :parse-stream :*trace*) dst-package)

      (loop :for aform :in forms
	    :do (when *trace*
		  (format *trace-output* "Injecting form ~A~%" aform))
	    :do (destructuring-bind (defun-sym name args &rest body)
		    aform
		  (declare (ignore defun-sym))
		  (intern (symbol-name
			   (compile name `(lambda ,args ,@body))) dst-package)))
      (loop :for (sym code) :in actions
	    :when sym
	    :do (when *trace* (format *trace-output* "Injecting definition for ~A~%" sym))
	    :and :do (intern (symbol-name
			      (compile sym `(lambda (data) (declare (ignorable data)) ,code))) dst-package)))))



(defun transform (tree &optional (depth 0))
  (if (and tree
	   (consp tree))
      (if (eq (first tree) ':action)
	  (progn
	    (when *trace*
	      (format *trace-output* "~AFound action ~A~%"  (make-string depth :initial-element #\Space) tree))
	    tree)
	  (let ((data (mapcar #'(lambda (tr) (transform tr (1+ depth)))
			      tree)))
	    (loop :for el :in data
		  :when (and (listp el)
			     (eq (first el) ':action)
			     (symbolp (third el)))
		  :do (let ((*package* (dst-package *context*))
			    (action (third el)))
			(when *trace*
			  (format *trace-output* "~&Applying action ~A to ~A~%" action data))
			(handler-case
			    (return-from transform
			      (funcall (symbol-function action) data))
			  (undefined-function (x)
			    (progn
			      (format *error-output* "missing definition for ~A: ~A~%" action x)
			      ;			      (break "~A in ~A" action *package*)
				    tree)))))
	    data))
      tree))