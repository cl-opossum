;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10; -*-
;;;
;;; package.lisp --- cl-opossum package definition file

;; Copyright (C) 2008 Utz-Uwe Haus <lisp@uuhaus.de>
;;
;; $Id$
;;
;; This code is free software; you can redistribute it and/or modify
;; it under the terms of the version 2.1 of the GNU Lesser General
;; Public License as published by the Free Software Foundation, as
;; clarified by the lisp prequel found in LICENSE.
;;
;; This code is distributed in the hope that it will be useful, but
;; without any warranty; without even the implied warranty of
;; merchantability or fitness for a particular purpose. See the GNU
;; Lesser General Public License for more details.
;;
;; Version 2.1 of the GNU Lesser General Public License is in the file
;; LICENSE that was distributed with this file. If it is not
;; present, you can access it from
;; http://www.gnu.org/copyleft/lgpl.txt (until superseded by a
;; newer version) or write to the Free Software Foundation, Inc., 59
;; Temple Place, Suite 330, Boston, MA 02111-1307 USA
;;
;; Commentary:

;; 

;;; Code:



(defpackage #:opossum
  (:use #:CL)
  ;; types
  (:export #:context)
  ;; special variables
  (:export #:*context* #:*trace*)
  ;; low-level user interface
  (:export #:generate-parser-file #:generate-parser-package
	   #:get-string-parser #:get-file-parser #:get-stream-parser
	   #:parse-file #:parse-string #:parse-stream
	   #:read-stream #:read-file)
  ;; high-level user interface
  (:export #:make-string-parser
	   #:make-file-parser
	   #:make-stream-parser)
  (:documentation
   "This package provides a parser generator for Parsing Expression Grammars (PEGs) for Common Lisp.

   The canonical project home page is @a[http://primaldual.de/cl-opossum/]{here}, the
   @a[http://git.or.cz/]{GIT repository} is hosted on @a[http://repo.or.cz/]{repo.or.cz} under the
   project @a[http://repo.or.cz/w/cl-opossum.git]{cl-opossum.git}. You can download a
   snapshot of the current master branch head @a[http://repo.or.cz/w/cl-opossum.git?a=snapshot]{here}.

   @begin[Installation]{section}
   To make use of this package you will need
   @begin{itemize}
    @item{A Common Lisp Environment. @a[http://sbcl.sourceforge.net/]{SBCL} is a good free candidate. See
          the @a[http://wiki.alu.org/Implementation]{ALU Wiki} for a list of choices.}
    @item{(Optionally) @a[http://www.gnu.org/emacs]{Emacs} with  @a[http://common-lisp.net/project/slime/]{SLIME} installed.}
    @item{The @a[http://www.cliki.net/asdf]{ASDF} System definition package. If you chose SBCL above then you already have it.}
    @item{This package, either as a @a[http://repo.or.cz/w/cl-opossum.git?a=snapshot]{snapshot} or through
         git by @a[git://repo.or.cz/cl-opossum.git]{git protocol} or @a[http://repo.or.cz/r/cl-opossum.git]{http}.}
   @end{itemize}

   If you are using @a[http://www.debian.org]{Debian GNU/Linux} you can fetch the prerequistites by
   @pre{apt-get install emacs slime sbcl}
   A one-step solution may be @a[http://www.gigamonkeys.com/lispbox/]{Lispbox}.

   If any of the above does not make sense to you, you may want to
   start learning more about Common Lisp by reading Peter Seibel's
   @em{Practical Common Lisp}, a great Lisp book suitable as
   online <a href=\"http://www.gigamonkeys.com/book/\">Common Lisp tutorial</a> 
   but also available in printed form with ISBN 978-1590592397.
   @end{section}

   @begin[Quick start]{section}
   If you want to get a quick start, load the package into your lisp and evaluate
   @begin{pre}
CL-USER> (asdf:operate 'asdf:load-op :opossum)
; registering #<SYSTEM OPOSSUM> as OPOSSUM
NIL
CL-USER> (setq *p* (opossum:make-string-parser \"example.peg\"))
\[lots of output scrolling by\]
CL-USER> (setq *t* (funcall *p* \"14+7*(1+1)*2\"))
...
CL-USER>(eval *t*)
   @end{pre}
   @end{section}
   
   @begin[Literature]{section}
   PEG parsers are a family of recursive descent parsers suitable to parsing context free grammars,
   in linear time if memoization is used.
   See @a[http://pdos.csail.mit.edu/~baford/packrat/]{Bryan Ford}'s home page for details.
   @end{section}
   @begin[Authors]{section}
   This code is released under the GNU Lesser Public License with Lisp clarifications and
   copyrighted by
   @a[http://www.math.uni-magdeburg.de/~haus/]{Utz-Uwe Haus}
   Please see the file @code{LICENSE} in the distribution for details.
   @end{section}

   "))



