;; -*- mode:lisp -*-
(in-package :opossum-system)
(eval-when (:compile-toplevel :load-toplevel :execute)
  (declaim (optimize (speed 0) (safety 3) (debug 3))))
(defun parse-file (f dst-package)
  (let ((opossum:*context* (make-instance 'opossum:context :start-index 0
					  :dst-package dst-package
					  :input (opossum::read-file f))))
    (funcall (|parse_program|) 0)))

(DEFUN |parse_program| ()
  (opossum::BUILD-PARSER-FUNCTION "program"
				  (opossum::SEQ (opossum::MANY (|parse_ws|))
						(opossum::MANY1
						 (|parse_rule|))
						(LIST ':ACTION NIL
						      '|metapeg_action320|))))

(DEFUN |parse_rule| ()
  (opossum::BUILD-PARSER-FUNCTION "rule"
				  (opossum::SEQ (|parse_id|)
						(opossum::MANY (|parse_ws|))
						(opossum::MATCH-STRING "<-")
						(opossum::MANY (|parse_ws|))
						(|parse_ordered-expr-list|)
						(opossum::MANY
						 (|parse_ws_or_nl|))
						(LIST ':ACTION NIL
						      '|metapeg_action321|))))
(DEFUN |parse_ordered-expr-list| ()
  (opossum::BUILD-PARSER-FUNCTION "ordered-expr-list"
				  (opossum::EITHER
				   (opossum::SEQ (|parse_expr-list|)
						 (opossum::MANY (|parse_ws|))
						 (opossum::MATCH-STRING "/")
						 (opossum::MANY (|parse_ws|))
						 (|parse_ordered-expr-list|)
						 (LIST ':ACTION NIL
						       '|metapeg_action322|))
				   (opossum::SEQ (|parse_expr-list|)
						 (LIST ':ACTION NIL
						       '|metapeg_action323|)))))
(DEFUN |parse_expr-list| ()
  (opossum::BUILD-PARSER-FUNCTION "expr-list"
				  (opossum::SEQ (|parse_expr|)
						(opossum::MANY
						 (opossum::SEQ
						  (opossum::MANY1
						   (|parse_ws|))
						  (|parse_expr-list|)))
						(LIST ':ACTION NIL
						      '|metapeg_action324|))))
(DEFUN |parse_expr| ()
  (opossum::BUILD-PARSER-FUNCTION "expr"
				  (opossum::EITHER
				   (opossum::SEQ (|parse_simple-expr|)
						 (opossum::MATCH-STRING "*")
						 (LIST ':ACTION NIL
						       '|metapeg_action325|))
				   (opossum::SEQ (|parse_simple-expr|)
						 (opossum::MATCH-STRING "+")
						 (LIST ':ACTION NIL
						       '|metapeg_action326|))
				   (opossum::SEQ (|parse_simple-expr|)
						 (opossum::MATCH-STRING "?")
						 (LIST ':ACTION NIL
						       '|metapeg_action327|))
				   (opossum::SEQ (|parse_simple-expr|)
						 (LIST ':ACTION NIL
						       '|metapeg_action328|)))))
(DEFUN |parse_simple-expr| ()
  (opossum::BUILD-PARSER-FUNCTION "simple-expr"
				  (opossum::EITHER
				   (opossum::SEQ (|parse_string|)
						 (LIST ':ACTION NIL
						       '|metapeg_action329|))
				   (|parse_action|)
				   (opossum::SEQ (opossum::MATCH-STRING "&")
						 (|parse_simple-expr|)
						 (LIST ':ACTION NIL
						       '|metapeg_action330|))
				   (opossum::SEQ (opossum::MATCH-STRING "@")
						 (|parse_id|)
						 (LIST ':action NIL
						       '|metapeg_action331|))
				   (opossum::SEQ (|parse_id|)
						 (LIST ':action NIL
						       '|metapeg_action332|))
				   (opossum::SEQ (|parse_bracketed-rule|)
						 (LIST ':action NIL
						       '|metapeg_action333|))
				   
				   (opossum::SEQ (opossum::MATCH-STRING "!")
						 (|parse_expr|)
						 (LIST ':action NIL
						       '|metapeg_action334|))
				   (opossum::SEQ (|parse_character-class|)
						 (LIST ':action NIL
						       '|metapeg_action335|))
				   (opossum::SEQ (opossum::MATCH-STRING ".")
						 (LIST ':action NIL
						       '|metapeg_action336|)))))
(DEFUN |parse_bracketed-rule| ()
  (opossum::BUILD-PARSER-FUNCTION "bracketed-rule"
				  (opossum::EITHER
				   (opossum::MATCH-STRING "()")
				   (opossum::SEQ (opossum::MATCH-STRING "(")
						 (opossum::MANY (|parse_ws|))
						 (|parse_ordered-expr-list|)
						 (opossum::MANY (|parse_ws|))
						 (opossum::MATCH-STRING ")")
						 (LIST ':action NIL
						       '|metapeg_action337|)))))
(DEFUN |parse_id| ()
  (opossum::BUILD-PARSER-FUNCTION "id"
				  (opossum::SEQ
				   (opossum::MANY1
				    (opossum::MATCH-CHAR
				     '(#\A #\B #\C #\D #\E #\F #\G #\H #\I
				       #\J #\K #\L #\M #\N #\O #\P #\Q #\R
				       #\S #\T #\U #\V #\W #\X #\Y #\Z #\a
				       #\b #\c #\d #\e #\f #\g #\h #\i #\j
				       #\k #\l #\m #\n #\o #\p #\q #\r #\s
				       #\t #\u #\v #\w #\x #\y #\z #\- #\_)))
				   (LIST ':action NIL
					 '|metapeg_action338|))))
(DEFUN |parse_character-class| ()
  (opossum::BUILD-PARSER-FUNCTION "character-class"
				  (opossum::SEQ (opossum::MATCH-STRING "[")
						(opossum::MANY1
						 (opossum::SEQ
						  (|parse_not_right_bracket|)
						  (opossum::MATCH-ANY-CHAR)))
						(opossum::MATCH-STRING "]")
						(LIST ':action NIL
						      '|metapeg_action339|))))
(DEFUN |parse_string| ()
  (opossum::BUILD-PARSER-FUNCTION "string"
				  (opossum::SEQ (opossum::MATCH-CHAR '(#\"))
						(opossum::MANY
						 (opossum::SEQ
						  (opossum::NEGATE
						   (opossum::MATCH-CHAR
						    '(#\")))
						  (opossum::MATCH-ANY-CHAR)))
						(opossum::MATCH-CHAR '(#\"))
						(LIST ':action NIL
						      '|metapeg_action340|))))
(DEFUN |parse_action| ()
  (opossum::BUILD-PARSER-FUNCTION "action"
				  (opossum::SEQ (opossum::MATCH-CHAR '(#\{))
						(opossum::MANY
						 (opossum::SEQ
						  (opossum::NEGATE
						   (opossum::MATCH-CHAR
						    '(#\})))
						  (opossum::MATCH-ANY-CHAR)))
						(opossum::MATCH-CHAR '(#\}))
						(LIST ':action NIL
						      '|metapeg_action341|
						      opossum:*context*))))
(DEFUN |parse_not_right_bracket| ()
  (opossum::BUILD-PARSER-FUNCTION "not_right_bracket"
				  (opossum::NEGATE
				   (opossum::MATCH-STRING "]"))))
(Defun |parse_comment| ()
  (opossum::BUILD-PARSER-FUNCTION "comment" (opossum::seq
					     (opossum::match-char '(#\#))
					     (opossum::many
					      (opossum::seq
					       (opossum::negate
						(opossum::match-char '(#\Newline)))
					       (opossum::match-any-char)))
					     (opossum::optional
					      (opossum::match-char '(#\Newline))))))
(DEFUN |parse_ws| ()
  (opossum::BUILD-PARSER-FUNCTION "ws" (opossum::either
					(opossum::MATCH-CHAR '(#\Space #\Tab))
					(|parse_comment|))))
(DEFUN |parse_nl| ()
  (opossum::BUILD-PARSER-FUNCTION "nl" (opossum::MATCH-CHAR '(#\Newline))))
(DEFUN |parse_ws_or_nl| ()
  (opossum::BUILD-PARSER-FUNCTION "ws_or_nl"
				  (opossum::EITHER (|parse_ws|)
						   (|parse_nl|)
						   )))

 
(defun |metapeg_action341| (data)
  (let* ((action-sans-{ (second data))
	 (action-code (coerce
		       (opossum::fix-escape-sequences
			(mapcar #'second action-sans-{))
		       'string))
	 (action-name (opossum::make-action-name)))
    (opossum::store-action opossum:*context*
			   `(,action-name ,action-code))
    `(list ':action nil ',action-name)))

(defun |metapeg_action340| (data)  `(opossum::match-string ,(coerce (opossum::fix-escape-sequences (mapcar #'second (second data))) 'string)))

(defun |metapeg_action339| (data)
  ;; FIXME: this is character-class matching and needs to use cl-ppcre
  `(opossum::match-char-class ,(coerce (mapcar #'second (second data)) 'string)))

(defun |metapeg_action338| (data)  (coerce (first data) 'string)  )
(defun |metapeg_action337| (data)  (third data)  )
(defun |metapeg_action336| (data)  (declare (ignore data)) `(opossum::match-any-char)  )
(defun |metapeg_action335| (data)  (first data)  )
(defun |metapeg_action334| (data)  `(opossum::negate ,(second data))  )
(defun |metapeg_action333| (data)  (first data)  )
(defun |metapeg_action332| (data)  
	`(,(opossum::make-name (first data)))
 )
(defun |metapeg_action331| (data)  `(opossum::match ,(second data))  )
(defun |metapeg_action330| (data)  `(opossum::follow ,(second data))  )
(defun |metapeg_action329| (data)  (first data)  )
(defun |metapeg_action328| (data)  (first data)  )
(defun |metapeg_action327| (data)  `(opossum::optional ,(first data))  )
(defun |metapeg_action326| (data)  `(opossum::many1 ,(first data))  )
(defun |metapeg_action325| (data)  `(opossum::many ,(first data))  )
(defun |metapeg_action324| (data)  (if (or (equal (second data) "") (null (second data)))
					     (first data)
					     (let ((tail (second (first (second data)))))
						  (if (equal (first tail) 'seq)
		   			              `(opossum::seq ,(first data) ,@(rest tail))
		   			              `(opossum::seq ,(first data) ,tail))))  )
(defun |metapeg_action323| (data)  (first data)  )
(defun |metapeg_action322| (data)  
(let ((tail (fifth data)))
	(if (equal (first tail) 'either)
	    `(opossum::either ,(first data) ,@(rest tail))
	    `(opossum::either ,(first data) ,(fifth data))))
 )
(defun |metapeg_action321| (data)  `(defun ,(opossum::make-name (first data)) ()
				     (opossum::build-parser-function ,(first data) ,(fifth data)))  )
(defun |metapeg_action320| (data) 
`(  ,@(second data))
 )