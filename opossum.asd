;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10; -*-
;;;
;;; opossum.asd --- Opossum systemd definition file

;; Copyright (C) 2008 Utz-Uwe Haus <lisp@uuhaus.de>
;;
;; $Id$
;;
;; This code is free software; you can redistribute it and/or modify
;; it under the terms of the version 2.1 of the GNU Lesser General
;; Public License as published by the Free Software Foundation, as
;; clarified by the lisp prequel found in LICENSE.
;;
;; This code is distributed in the hope that it will be useful, but
;; without any warranty; without even the implied warranty of
;; merchantability or fitness for a particular purpose. See the GNU
;; Lesser General Public License for more details.
;;
;; Version 2.1 of the GNU General Public License is in the file
;; LICENSE that was distributed with this file. If it is not
;; present, you can access it from
;; http://www.gnu.org/copyleft/lgpl.txt (until superseded by a
;; newer version) or write to the Free Software Foundation, Inc., 59
;; Temple Place, Suite 330, Boston, MA 02111-1307 USA
;;
;; Commentary:

;; ASDF definitions for the opossum system, including bootstrapping logic.

;;; Code:



(defpackage #:opossum-system
  (:use #:cl #:asdf))
(in-package #:opossum-system)

;; We provide a pegparser-boot.lisp file for bootstrapping purposes.
;; and use it to generate pegparser.lisp in the perform methods for
;; bootstrapped-file. 

(defclass bootstrapped-file (cl-source-file) ()
  (:documentation ""))
;; and use it to 

(defsystem #:opossum
  :description "?"
  :version     "0"
  :author      "Utz-Uwe Haus <lisp@uuhaus.de>"
  :license     "ask me"
  :depends-on  ("cl-ppcre")
  :components  ((:file "package")
		(:file "pegutils" :depends-on ("package"))
		;; code loaded at system build time to create pegparser lisp file using
		;; pegparser-boot.lisp
		(:file "pegparser-boot" :depends-on ("package" "pegutils"))
		(:file "bootstrap" :depends-on ("pegparser-boot"))
		(:file "pegparser"
		       :in-order-to ((compile-op (load-op "bootstrap"))))
		(:file "entrypoints" :depends-on ("pegparser" "package" "pegutils"))))

