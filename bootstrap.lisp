;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10; -*-
;;;
;;; bootstrap.lisp --- Invoke magic to create pegparser.lisp source

;; Copyright (C) 2008 Utz-Uwe Haus <lisp@uuhaus.de>
;; $Id$
;;
;; This code is free software; you can redistribute it and/or modify
;; it under the terms of the version 2.1 of the GNU Lesser General
;; Public License as published by the Free Software Foundation, as
;; clarified by the lisp prequel found in LICENSE.
;;
;; This code is distributed in the hope that it will be useful, but
;; without any warranty; without even the implied warranty of
;; merchantability or fitness for a particular purpose. See the GNU
;; Lesser General Public License for more details.
;;
;; Version 2.1 of the GNU Lesser General Public License is in the file
;; LICENSE that was distributed with this file. If it is not
;; present, you can access it from
;; http://www.gnu.org/copyleft/lgpl.txt (until superseded by a
;; newer version) or write to the Free Software Foundation, Inc., 59
;; Temple Place, Suite 330, Boston, MA 02111-1307 USA
;;
;; Commentary:

;; This file is loaded to bootstrap ourselves: it generates pegparser.lisp
;; from opossum.peg using pegparser-boot.lisp.

;;; Code:



(in-package #:opossum-system)


(eval-when (:compile-toplevel :load-toplevel :execute)
  (format *trace-output* ";; bootstrap.lisp is creating pegparser.lisp~%")
  (opossum:generate-parser-file "opossum.peg" :opossum
				"pegparser.lisp"
				:parse-file-fun #'opossum-system::parse-file)
  (format *trace-output* ";; done creating pegparser.lisp~%"))