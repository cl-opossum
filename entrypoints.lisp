;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10; -*-
;;;
;;; entrypoints.lisp --- parser generator entry points

;; Copyright (C) 2008 Utz-Uwe Haus <lisp@uuhaus.de>
;; $Id$
;;
;; This code is free software; you can redistribute it and/or modify
;; it under the terms of the version 2.1 of the GNU Lesser General
;; Public License as published by the Free Software Foundation, as
;; clarified by the lisp prequel found in LICENSE.
;;
;; This code is distributed in the hope that it will be useful, but
;; without any warranty; without even the implied warranty of
;; merchantability or fitness for a particular purpose. See the GNU
;; Lesser General Public License for more details.
;;
;; Version 2.1 of the GNU General Public License is in the file
;; LICENSE that was distributed with this file. If it is not
;; present, you can access it from
;; http://www.gnu.org/copyleft/lgpl.txt (until superseded by a
;; newer version) or write to the Free Software Foundation, Inc., 59
;; Temple Place, Suite 330, Boston, MA 02111-1307 USA
;;
;; Commentary:

;; This file contains code for the opossum package that depends on
;; the parser part already being compiled to avoid undefined symbol warnings

;;; Code:



(in-package #:opossum)

;; low-level macros
(defmacro get-string-parser (pkg)
  "Return the parse-string function in the package PKG."
  `(let ((*package* ,pkg))
    #'parse-string))

(defmacro get-file-parser (pkg)
  "Return the parse-file function in the package PKG."
  `(let ((*package* ,pkg))
    #'parse-file))

(defmacro get-stream-parser (pkg)
  "Return the parse-stream function in the package PKG."
  `(let ((*package* ,pkg))
    #'parse-stream))


;;; parser entry point
(defun make-string-parser (grammarfile &rest args
			   &key start-rule dst-package )
  "Return a function of 1 argument, a string, that parses according to GRAMMARFILE, starting at rule named HEAD. The parser is instantiated in package DST-PACKAGE."
  (declare (ignore start-rule dst-package))
  (get-string-parser
   (apply #'opossum:generate-parser-package grammarfile args)))

(defun make-file-parser (grammarfile start-rule &key (dst-package (make-default-dst-package grammarfile)))
  "Return a function of 1 argument, a file, that parses according to GRAMMARFILE, starting at rule named HEAD. The parser is instantiated in package DST-PACKAGE."
  (get-file-parser (opossum:generate-parser-package grammarfile
						    :dst-package dst-package
						    :start-rule start-rule)))

(defun make-stream-parser (grammarfile start-rule &key (dst-package (make-default-dst-package grammarfile)))
  "Return a function of 1 argument, a stream, that parses according to GRAMMARFILE, starting at rule named HEAD. The parser is instantiated in package DST-PACKAGE."
  (get-stream-parser (opossum:generate-parser-package grammarfile
						      :dst-package dst-package
						      :start-rule start-rule)))

